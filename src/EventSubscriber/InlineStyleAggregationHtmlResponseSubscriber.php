<?php

namespace Drupal\inline_style_aggregation\EventSubscriber;

use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Response subscriber to handle HTML responses.
 */
class InlineStyleAggregationHtmlResponseSubscriber implements EventSubscriberInterface {

  /**
   * Processes attachments for HtmlResponse responses.
   *
   * @param \Symfony\Component\HttpKernel\Event\ResponseEvent $event
   *   The event to process.
   */
  public function response(ResponseEvent $event) {
    $response = $event->getResponse();

    $allowed_response_classes = [
      'Drupal\big_pipe\Render\BigPipeResponse',
      'Drupal\Core\Render\HtmlResponse',
    ];
    if (in_array(get_class($response), $allowed_response_classes)) {
      $crawler = new Crawler($response->getContent());

      $style = '';
      $tags = $crawler->filter('body style');

      foreach ($tags as $tag) {
        foreach ($tag->childNodes as $node) {
          $style .= $node->data;
        }
        $tag->parentNode->removeChild($tag);
      }

      $heads = $crawler->filterXPath('//html/head');
      if ($heads->count()) {
        foreach ($heads as $head) {
          $element = $head->parentNode->ownerDocument->createElement('style', $style);
          $element->setAttribute('generated-by', 'inline_style_aggregation');
          $head->appendChild($element);
        }
        $html = '';
        foreach ($crawler as $domElement) {
          $html .= $domElement->ownerDocument->saveHTML($domElement);
        }
        $response->setContent($html);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [];

    $events[KernelEvents::RESPONSE][] = ['response', -10000];

    return $events;
  }

}
